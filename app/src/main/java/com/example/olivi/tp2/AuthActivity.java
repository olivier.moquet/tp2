package com.example.olivi.tp2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.json.*;
import java.io.*;

public class AuthActivity extends AppCompatActivity {

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while(i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }
    EditText name,pwd;
    public String authverif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        name = (EditText)findViewById(R.id.textName);
        pwd = (EditText)findViewById(R.id.textPwd);
        final TextView result = (TextView)findViewById(R.id.textRes);
        Button auth = (Button)findViewById(R.id.button_auth);
        auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Threadbutton thread1 = new Threadbutton();
                thread1.name = name.getText().toString();
                thread1.pwd =pwd.getText().toString();
                thread1.start();
                final String res = thread1.result;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                                         try {
                                                JSONObject obj1= new JSONObject(res);
                                                authverif=obj1.getString("authenticated");

                                         } catch (JSONException e) {
                                             e.printStackTrace();
                                         }
                                         result.setText(authverif);
                    }
                });
            }});
    }



}
