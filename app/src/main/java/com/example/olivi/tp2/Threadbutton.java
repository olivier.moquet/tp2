package com.example.olivi.tp2;

import android.util.*;
import android.widget.EditText;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class Threadbutton extends Thread {
    public String name;
    public String pwd;
    public String result;

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while(i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }

    public void run(){
        URL url = null;

        try {
            url = new URL("https://httpbin.org/basic-auth/bob/sympa");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String basicAuth = "Basic " + Base64.encodeToString((name+":"+pwd).getBytes(),
                    Base64.NO_WRAP);
            urlConnection.setRequestProperty ("Authorization", basicAuth);
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                this.result = readStream(in);
                Log.i("JFL", this.result);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
